#include <fcntl.h>

// 620i/Varian_620i_Reference_Manual_Mar68.pdf

#include "varian.h"


// Varian mode settings
bool mode18 = false;


// Memory
word16 memory[MEM_SZ];

// Registers
word16 P; // Program counter
word16 A; // Accumulator
word16 B; // Double length accumulator
word16 X; // Index register

// Buffer registers
word16 R; // Operand register
word16 U; // Instruction register
word5 S; // Shift register
word16 L; // Memory address register
word16 W; // Memory word register

word1 OF; // Overflow

word1 SS1; // Sense switch 1
word1 SS2; // Sense switch 2
word1 SS3; // Sense switch 3


bool haveKbChar = false;
char kbChar;

char * stepStr[4] = { "XFR", "INC", "CMP", "DEC" };
char * regStr[8] = { "...", "A..", ".B.", "AB.", "..X", "A.X", ".BX", "ABX" };

// simh boiler plate
t_stat cpuReset (DEVICE * dptr);
t_stat boot (int32 unit_num, DEVICE *dptr);

/* Memory examine */
//  t_stat examine_routine (t_val *eval_array, t_addr addr, UNIT *uptr, int32
//  switches) 
//  Copy  sim_emax consecutive addresses for unit uptr, starting
//  at addr, into eval_array. The switch variable has bit<n> set if the n'th
//  letter was specified as a switch to the examine command.
// Not true...
    
t_stat cpuEx (t_value *vptr, t_addr addr, UNUSED UNIT * uptr, UNUSED int32 sw) { 
  if (addr>= MEM_SZ)
    return SCPE_NXM;
  if (vptr != NULL) {
    *vptr = memory[addr] & MASK16;
  }
  return SCPE_OK;
}

/* Memory deposit */

static t_stat cpuDe (t_value val, t_addr addr, UNUSED UNIT * uptr, UNUSED int32 sw) {
  if (addr >= MEM_SZ)
    return SCPE_NXM;
  memory[addr] = val & MASK16;
  return SCPE_OK;
}


REG registers[] = {
  { 
    "P", /* name */
    & P, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "program counter", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (P) /* structure size */
  },
  { 
    "A", /* name */
    & A, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "accumulator", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (A) /* structure size */
  },
  { 
    "B", /* name */
    & B, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "double length accumlator", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (B) /* structure size */
  },
  { 
    "X", /* name */
    & X, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "index register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (X) /* structure size */
  },
  { 
    "R", /* name */
    & R, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "operand register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (R) /* structure size */
  },
  { 
    "U", /* name */
    & U, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "instruction register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (U) /* structure size */
  },
  { 
    "S", /* name */
    & S, /* location */
    8, /* radix */
    5, /* width */
    0, /* offset */
    1, /* depth */
    "shift register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (S) /* structure size */
  },
  { 
    "L", /* name */
    & L, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "memory address register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (L) /* structure size */
  },
  { 
    "W", /* name */
    & W, /* location */
    8, /* radix */
    16, /* width */
    0, /* offset */
    1, /* depth */
    "memory word register", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (W) /* structure size */
  },
  { 
    "SS1", /* name */
    & SS1, /* location */
    8, /* radix */
    1, /* width */
    0, /* offset */
    1, /* depth */
    "sense switch 1", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (SS1) /* structure size */
  },
  { 
    "SS2", /* name */
    & SS2, /* location */
    8, /* radix */
    1, /* width */
    0, /* offset */
    1, /* depth */
    "sense switch 2", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (SS2) /* structure size */
  },
  { 
    "SS3", /* name */
    & SS3, /* location */
    8, /* radix */
    1, /* width */
    0, /* offset */
    1, /* depth */
    "sense switch 3", /* desc */
    NULL, /* bit fields */
    0, /* flags */
    0, /* current queue pointer */
    sizeof (SS3) /* structure size */
  },
  { 
    NULL
  }
};

/* scp Debug flags */

#define DBG_TRACE         (1U << 0)      ///< instruction trace

static DEBTAB cpuDT[] = {
  { "TRACE",       DBG_TRACE,       NULL },
  { NULL,          0,               NULL }
};

// No value, DEV only, in "show"
#define MTAB_dev_novalue     MTAB_XTD | MTAB_VDV

static MTAB cpuMod[] = {
  {
    MTAB_dev_novalue,               /* Mask               */
    0,                              /* Match              */
    "CONFIG",                       /* Print string       */
    "CONFIG",                       /* Match string       */
    NULL,                           /* Validation routine */
    NULL,                           /* Display routine    */
    NULL,                           /* Value descriptor   */
    NULL                            /* Help               */
  },
  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};



UNIT cpuUnit[1] = { UDATA (NULL, UNIT_FIX|UNIT_BINK, MEM_SZ) };

DEVICE cpuDev = {
 "varian", /* name */
 cpuUnit,   /* units */
 registers, /* registers */
 cpuMod, /* modifiers */   // XXX
 1, /* number of CPUs */
 8, /* address radix */
 16, /* address width */
 1, /* address increment */
 8, /* data radix */
#if MODE18
 18, /* data width */
#else
 16, /* data width */
#endif
 cpuEx, /* examine routine */
 cpuDe, /* deposit routine */
 cpuReset, /* reset routine */
 boot, /* boot routine */
 NULL, /* attach routine */
 NULL, /* detach routine */
 NULL, /* ctx */   // XXX
 DEV_DEBUG, /* flags */
 0, /* debug control flags */  // XXX
 cpuDT, /* debug flag names */  // XXX
 NULL, /* memory size change */
 "CPU", /* logical name */
 NULL, /* help routine */
 NULL, /* attach help routine */
 NULL, /* help context */
 NULL /* device description */   // XXX
};

#define UNIT_FLAGS ( UNIT_FIX | UNIT_ATTABLE | UNIT_ROABLE | UNIT_DISABLE | UNIT_IDLE )

UNIT hsptUnit [1] = {
  {UDATA (NULL, UNIT_FLAGS, 0)}
};

static DEBTAB hsptDT [] = {
  { "TRACE",  DBG_TRACE,  NULL },
  { NULL,     0,          NULL }
};

t_stat hsptReset (UNUSED DEVICE * dptr) {
    return SCPE_OK;
  }

DEVICE hsptDev = {
 "HSPT", /* name */
 hsptUnit,   /* units */
 NULL, /* registers */
 NULL, /* modifiers */   // XXX
 1, /* number of units */
 8, /* address radix */
 16, /* address width */
 1, /* address increment */
 8, /* data radix */
#if MODE18
 18, /* data width */
#else
 16, /* data width */
#endif
 NULL, /* examine routine */
 NULL, /* deposit routine */
 hsptReset, /* reset routine */
 NULL, /* boot routine */
 NULL, /* attach routine */
 NULL, /* detach routine */
 NULL, /* ctx */   // XXX
 DEV_DEBUG, /* flags */
 0, /* debug control flags */  // XXX
 hsptDT, /* debug flag names */  // XXX
 NULL, /* memory size change */
 "CPU", /* logical name */
 NULL, /* help routine */
 NULL, /* attach help routine */
 NULL, /* help context */
 NULL /* device description */   // XXX
};

static void varianInit (void);
static void varianExit (void);
void (*sim_vm_init) (void) = & varianInit;
void (*sim_vm_exit) (void) = & varianExit;

char sim_name[] = "varian";
REG *sim_PC = &registers[0]; // program counter
int32 sim_emax = 2; // maximum number of words in an instruction or data item
DEVICE *sim_devices[] = {
  & cpuDev,
  & hsptDev, // high speed paper tape
  NULL,
}; 

t_stat sim_load (FILE *fptr, const char *buf, const char *fnam, t_bool flag) {
  return SCPE_ARG;
}


t_stat fprint_sym (FILE *ofile, t_addr addr, t_value *val, UNIT *uptr, int32 switch_) {
  return SCPE_ARG;
}

t_stat parse_sym (const char *cptr, t_addr addr, UNIT *uptr, t_value *val, int32 switch_) {
  return SCPE_ARG;
}


const char *sim_stop_messages[SCPE_BASE] = { NULL, };


t_stat cpuReset (DEVICE * dptr) {
  P = 0;
  OF = 0;
  return SCPE_OK;
}

static t_stat lgo (UNUSED int32 arg, const char * buf);

static CTAB varianCmds[] = {
  {"LGO", lgo, 0, "Load paper tape and go\n", 0, NULL },
  { NULL, NULL, 0, NULL, NULL, NULL}
}; // varianCmds


static void varianInit (void) {
  sim_vm_cmd = varianCmds;
}


static void varianExit (void) {
}

t_stat boot (int32 unit_num, DEVICE *dptr) {
  return SCPE_ARG;
}


////////////////////////////////////////////////////////////////////////////////
//
// lgo
//   lgo file loadAddress startAddress
//

static t_stat lgo (UNUSED int32 arg, const char * buf) {
  char filename[512];
  uint load, start;
  int cnt = sscanf (buf, "%512s %o %o\n", filename, & load, & start);
  if (cnt != 3) {
    sim_printf ("lgo file loadAddress startAddress\n");
    return SCPE_ARG;
  }
  if (load >= MEM_SZ) {
    sim_printf ("loadAddress > memory size\n");
    return SCPE_ARG;
  }
  if (start >= MEM_SZ) {
    sim_printf ("startAddress > memory size\n");
    return SCPE_ARG;
  }
  int rc = open (filename, O_RDONLY);
  if (rc < 0) {
    int e = errno;
    sim_printf ("open returned %d %s\n", e, strerror (e));
    return SCPE_ARG;
  }
#if 0

  unsigned char byte;

  enum state_e { FND_LDR }

  // Skip over label: look for channel 8 bit (leader)
  enum state_e state = FND_LDR
  while (1) {
    ssize_t nr = read (0, & byte, 1);
    if (nr != 1)
      break;
    switch (state) {

      case FND_LAB:
        if ((byte & 0200) == 0200)
          state = SKP_LDR;
        continue;

      case SKP_LDR:
        if ((byte & 0200) == 0200)
          continue;
        if (byte == 0400
#endif
    return SCPE_ARG;
}

////////////////////////////////////////////////////////////////////////////////
//
// fault
//

t_stat fault (enum FLT_CODE fltCode) {
 sim_printf ("fault %d P %06o L %06o U %06o\n", fltCode, P, L, U);
 if (fltCode == FLT_UNIMP)
    return SCPE_NOFNC;
 return SCPE_ARG;
}

////////////////////////////////////////////////////////////////////////////////
//
// Add/Sub
//

word16 add16b (word16 op1, word16 op2, word1 carryin, word1 * of) {
// See: https://en.wikipedia.org/wiki/Two%27s_complement#Addition

    // 17 bit arithmetic for the above N+1 algorithm
    word18 op1e = op1 & MASK16;
    word18 op2e = op2 & MASK16;
    word18 ci   = carryin ? 1 : 0;

    // extend sign bits
    if (op1e & SIGN16)
      op1e |= BIT17;
    if (op2e & SIGN16)
      op2e |= BIT17;

    // Do the math
    word18 res = op1e + op2e + ci;

    // Extract the overflow bits
    bool r17 = (res & BIT17)  ? true : false;
    bool r16 = (res & SIGN16) ? true : false;

    // Extract the carry bit
    bool r18 = res & BIT18 ? true : false;

     // Truncate the result
    res &= MASK16;

    // Check for overflow
    if (of && (r17 ^ r16))
      * of = 1;

    return (word16) res;
}

word16 sub16b (word16 op1, word16 op2, word1 carryin, word1 * of) {
// See: https://en.wikipedia.org/wiki/Two%27s_complement#Addition

    // 17 bit arithmetic for the above N+1 algorithm
    word18 op1e = op1 & MASK16;
    word18 op2e = op2 & MASK16;
   // Note that carryin has an inverted sense for borrow
    word18 ci   = carryin ? 0 : 1;

    // extend sign bits
    if (op1e & SIGN16)
      op1e |= BIT17;
    if (op2e & SIGN16)
      op2e |= BIT17;

    // Do the math
    word18 res = op1e - op2e - ci;

    // Extract the overflow bits
    bool r17 = (res & BIT17)  ? true : false;
    bool r16 = (res & SIGN16) ? true : false;

    // Extract the carry bit
    bool r18 = res & BIT18 ? true : false;

     // Truncate the result
    res &= MASK16;

    // Check for overflow
    if (of && (r17 ^ r16))
      * of = 1;

    return (word16) res;
}

///////////////////////////////////////////////////////////////////////////////
//
// computeAddress
//

// Single word addressing instruction -- read operand
void computeAddress (void) {
  uint mode = (U >> 9) & 07; // Extract addressing mode
  uint addr = U & 0777; // Extract address
  switch (mode) {
    case 00: // 0XX Direct address to 2048
    case 01: 
    case 02: 
    case 03: 
      addr = U & 03777;
      L = addr;
      return;

    case 04: // Relative -- add A field to P
      L = (addr + P) & MASK16;
      return;

    case 05: // Relative -- add A field to X
      L = (addr + X) & MASK16;
      return;

    case 06: // Relative -- add A field to B
      L = (addr + B) & MASK16;
      return;

    case 07: // Indirect
      do {
        L = addr;
        W = memory[L];
        addr = W;
        if ((addr & BIT15) == 0)
          break;
        addr &= MASK15;
      } while (1);
      L = addr & MASK16;
      return;
  }
}

///////////////////////////////////////////////////////////////////////////////
//
// High speed paper tape reader
//

// EXC 037  100037 Connect punch to BIC
// EXC 437  100437 Stop Reader
// EXC 537  100537 Start Reader
// EXC 637  100637 Punch Buffer
// EXC 737  100737 Read One Character
//
// OAR 37   103137 Load Buffer from A Register
// OBR 37   103237 Load Buffer from A Register
// OME 37   103037 Load Buffer from Memory
// INA 37   102137 Read Buffer into A Register
// INB 37   102137 Read Buffer into B Register
// IME 37   102037 Read Buffer into Memory
// CIA 37   102537 Read Buffer into Cleared A Register
// CIB 37   102637 Read Buffer into Cleared B Register
//
// SEN 537  101527 Sense Buffer Ready

// XXX Only one Sense Buffer Ready? The unit must keep track of
// start reader / punch buffer to track which sense.

char hsptBuf;
// XXX hack for bootstrap
bool hsptStart = false;

t_stat hsptExc (uint cmd) {
// cmd
//  0 connect punch to BIC
//  4 stop reader
//  5 start reader
//  6 punch buffer
//  7 read one character
  if (cmd == 5) { // Start Reader
    sim_debug (DBG_TRACE, & cpuDev, "  Start high speed reader\n");
    if (! hsptUnit[0].fileref) {
      sim_printf ("Start paper tape reader with no tape\n");
      return SCPE_ARG;
    }
    if (feof (hsptUnit[0].fileref)) {
      sim_printf ("Start paper tape reader at EOF\n");
      return SCPE_ARG;
    }
    hsptStart = true;
    return SCPE_OK;
  }
  return fault (FLT_UNIMP);
}

t_stat hsptSen (uint cmd, word1 * data) {
  if (cmd == 5) { // Sense buffer ready
    sim_debug (DBG_TRACE, & cpuDev, "  Sense high speed tape buffer ready\n");
    if (! hsptUnit[0].fileref) {
      sim_printf ("Sense paper tape reader with no tape\n");
      * data = 0;
      return SCPE_OK;
    }
    if (feof (hsptUnit[0].fileref)) {
      sim_printf ("Sense paper tape reader at EOF\n");
      * data = 0;
    }
    * data = 1;
    return SCPE_OK;
  }
  return fault (FLT_ILL);
}

t_stat hsptRead (word16 * data) {
  sim_debug (DBG_TRACE, & cpuDev, "  Read high speed tape\n");
  if (! hsptUnit[0].fileref) {
    sim_printf ("Read paper tape reader with no tape\n");
    return SCPE_ARG;
  }
  if (feof (hsptUnit[0].fileref)) {
    sim_printf ("Read paper tape reader at EOF\n");
    return SCPE_ARG;
  }
  unsigned char byte;
  while (1) {
    size_t nr = fread (& byte, sizeof (byte), 1, hsptUnit[0].fileref);
    if (nr != sizeof (byte)) {
     sim_printf ("warn: fread returned %lu, expected %lu\n", nr, sizeof (byte));
    }
    if (! hsptStart)
      break;
    if (byte == 0100) {
      hsptStart = false;
      break;
    }
  }
  sim_debug (DBG_TRACE, & cpuDev, "  Read %03o\n", byte);
  * data = byte;
  return SCPE_OK;
}

t_stat hsptWrite (word16 data) {
  return fault (FLT_UNIMP);
}

///////////////////////////////////////////////////////////////////////////////
//
// Operator console
//

t_stat opconExc (uint cmd) {
// cmd
//   1 Connect write register to BIC
//   2 Connect read register to BIC
//   4 Initialize
//
  return fault (FLT_UNIMP);
}

t_stat opconSen (uint cmd, word1 * data) {
  if (cmd == 01) { // Sense Write Register Ready
    * data = 1; // XXX
    return SCPE_OK;
  }
  if (cmd == 02) { // Sense Read Register Ready
    * data = haveKbChar ? 1 : 0;
    return SCPE_OK;
  }
  return fault (FLT_DEVCMD);
}

t_stat opconRead (word16 * data) {
  if (! haveKbChar)
    sim_printf ("warn: opconRead empty buffer\n");
  * data = kbChar;
  haveKbChar = false;
  return SCPE_OK;
}

t_stat opconWrite (word16 data) {
  sim_putchar (data & 0177);
  return SCPE_OK;
}

////////////////////////////////////////////////////////////////////////////////
//
// I/O instruction dispatch
//

t_stat ioExc (word9 xyy) {
  uint cmd = (xyy >> 6) & 07;
  uint devno = xyy & 077;
  
  switch (devno) {
    case 001: // console tty
      return opconExc (cmd);
    case 037: // high speed paper tape
      return hsptExc (cmd);
    default:
      return fault (FLT_DEVNO);
   }
}

t_stat ioSen (word9 xyy, word1 * data) {
  uint cmd = (xyy >> 6) & 07;
  uint devno = xyy & 077;
  
  switch (devno) {
    case 001: // console tty
      return opconSen (cmd, data);
    case 037: // high speed paper tape
      return hsptSen (cmd, data);
    default:
      return fault (FLT_DEVNO);
   }
}

t_stat ioRead (word6 devno, word16 * data) {
  switch (devno) {
    case 001: // console tty
      return opconRead (data);
    case 037: // high speed paper tape
      return hsptRead (data);
    default:
      return fault (FLT_DEVNO);
   }
}

t_stat ioWrite (word6 devno, word16 data) {
  switch (devno) {
    case 001: // console tty
      return opconWrite (data);
    case 037: // high speed paper tape
      return hsptWrite (data);
    default:
      return fault (FLT_DEVNO);
   }
}

////////////////////////////////////////////////////////////////////////////////
///
/// sim_instr
///


// single word addressing format
//
//  15 14 13 12    11 10 9   8 7 6 5 4 3 2 1 0
//  Op Code        Mode      Address
//
//  Mode
//   0XX   Direct address to 2048
//   100   Relative -- add A field to P
//   101   Index (X) -- add A field to X
//   110   Index (B) -- add A field to B
//   111   Indirect
//
//  Indirect address
//   Bit 15 -- additional indirection
//   Bits 14 - 0: address
//
// Nonaddressing
//
//  15 14 12 12    11 10 9   8 7  5 4 3 2 1 0
//  Class code     Op. Cod   Definition
//
// Two word instructions
//
//  15 14 12 12    11 10 9   8 7  5 4 3 2 1 0
//  Class code     Op. Cod   Condition
//
//   15  14 13 12 11 10 9 8 7 6 5 4 3 2 1 0
//   I   Address
//
//   I Indirect address flag
//  
// Immediate instructions
//
//  15    14 13 12   11 10 0   8 7 6 5 4 3     2 1 0
//  0     0          6         Op. Cod         0
// 
//  15 14 13 12 11 10 9 8 7 6 5 4 3 2 1
//  Operand
//
// Macro instructions
//  15 14 13 12   0
//  11 10 9       5
//  8             0 execute unconitionally; 1 execute if overflow st
//  7 6           0 0 transfer
//                0 1 increment
//                1 0 complement
//                1 1 decrement
//  5 4 3         XBA source
//  2 1 0         XBA destination

t_stat sim_instr (void) {
  int reason = 0;
  do {
    if (sim_interval <= 0) {
      reason = sim_process_event ();
      if (reason)
        break;
    }
    sim_interval --;

    if (! haveKbChar) {
      t_stat poll = sim_poll_kbd ();
      if (poll != SCPE_OK) {
        haveKbChar = true;
        kbChar = (char) (poll - SCPE_KFLAG);
      }
    }

    L = P; // Copy program counter to memory address register
    W = memory[L]; // Fetch from mmory
    U = W; // Load instruction register
    sim_debug (DBG_TRACE, & cpuDev, "P %06o U %06o\n", P, U);
    P = (P + 1) & MASK16; // Increment PC
  
execute:
    uint opCode = (U >> 12) & 017; // Extract the op code in bits 15-12

    switch (opCode) {
      case 000: { // Control group
        uint mode = (U >> 9) & 07; // Extract sub-opcode
        uint submode = U & 0777; // sub-sub-opcode

        switch (mode) {
          case 00: // HLT
            reason = SCPE_STOP;
            break;
 
          case 01: // Jump instruction group
                   //    JMP JOF JAP JAN JAZ JBZ JXZ JSS1 JSS2 JSS3
          case 02: // Jump and mark instruction group
                   //    JMPM JOFM JANM JAPM JAZM JBZM JXZM JS1M JS2M JS3M
          case 03: // Execute instruction group
                   //    XEC XOF XAP XAN XAZ XBZ XXC XS1 XS2 XS3

// is an indirect jump address 
// chased before or after the conditional check?
// After, 620i/Varian_620i_Reference_Manual_Mar68.pdf, pg 61, 3-34 fig 3-9.

//    8   SS3
//    7   SS2
//    6   SS1
//    5   X == 0
//    4   B == 0
//    3   A == 0
//    2   A < 0
//    1   A >= 0
//    0   OF == 1

            L = P; // Copy program counter to memory address register
            W = memory[L]; // Fetch from mmory
            P = (P + 1) & MASK16; // Increment PC

            word16 jmpAddr = W;

            if (mode == 1)
              sim_debug (DBG_TRACE, & cpuDev, "  Jump group, msk %03o, addr %06o\n", U & 0777, jmpAddr);
            else if (mode == 2)
              sim_debug (DBG_TRACE, & cpuDev, "  Jump and mark group, msk %03o, addr %06o\n", U & 0777, jmpAddr);
            else
              sim_debug (DBG_TRACE, & cpuDev, "  Execute group, msk %03o, addr %06o\n", U & 0777, jmpAddr);

            if (U & BIT8) // SS3
              if (SS3 == 0)
                sim_debug (DBG_TRACE, & cpuDev, "  SS3 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  SS3 pass\n");

            if (U & BIT7) // SS2
              if (SS2 == 0)
                sim_debug (DBG_TRACE, & cpuDev, "  SS2 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  SS2 pass\n");

            if (U & BIT6) // SS1
              if (SS1 == 0)
                sim_debug (DBG_TRACE, & cpuDev, "  SS1 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  SS1 pass\n");

            if (U & BIT5) // X == 0
              if (X != 0)
                sim_debug (DBG_TRACE, & cpuDev, "  X == 0 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  X == 0 pass\n");

            if (U & BIT4) // B == 0
              if (B != 0)
                sim_debug (DBG_TRACE, & cpuDev, "  B == 0 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  B == 0 pass\n");

            if (U & BIT3) // A == 0
              if (A != 0)
                sim_debug (DBG_TRACE, & cpuDev, "  A != 0 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  A != 0 pass\n");

            if (U & BIT2) // A < 0
              if ((A & BIT15) == 0)
                sim_debug (DBG_TRACE, & cpuDev, "  A < 0 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  A < 0 pass\n");

            if (U & BIT1) // A >= 0
              if ((A & BIT15) != 0)
                sim_debug (DBG_TRACE, & cpuDev, "  A >= 0 fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  A >= 0 pass\n");

            if (U & BIT0) // OF == 0
              if (OF)
                sim_debug (DBG_TRACE, & cpuDev, "  OF fail\n");
              else
                sim_debug (DBG_TRACE, & cpuDev, "  OF pass\n");


            if (U & BIT8) // SS3
              if (SS3 == 0)
                break;
            if (U & BIT7) // SS2
              if (SS2 == 0)
                break;
            if (U & BIT6) // SS1
              if (SS1 == 0)
                break;
            if (U & BIT5) // X == 0
              if (X != 0)
                break;
            if (U & BIT4) // B == 0
              if (B != 0)
                break;
            if (U & BIT3) // A == 0
              if (A != 0)
                break;
            if (U & BIT2) // A < 0
              if ((A & BIT15) == 0)
                break;
            if (U & BIT1) // A >= 0
              if ((A & BIT15) != 0)
                break;
            if (U & BIT0) // OF == 0
              if (OF)
                break;

            while (jmpAddr & BIT15) {
              L = jmpAddr;
              W = memory[L];
              jmpAddr = W & MASK16;
            }

            if (mode == 1) { // Jump
              sim_debug (DBG_TRACE, & cpuDev, "  jump to %06o\n", jmpAddr);
              P = jmpAddr;
              break;
            }

            if (mode == 2) { // Jump and mark
              sim_debug (DBG_TRACE, & cpuDev, "  jump-and-mark to %06o\n", jmpAddr);
              L = jmpAddr;
              W = P;
              memory[L] = W;
              P = (jmpAddr + 1) & MASK16;
              break;
            }

            /* mode == 3 */ { // Execute
              sim_debug (DBG_TRACE, & cpuDev, "  execute %06o\n", jmpAddr);
              L = jmpAddr;
              W = memory[L];
              U = W;
              goto execute;
            }

          case 04: // shift instruction group

            if ((submode & 0740) == 0000) { // ASLB
              word16 bsave = B;
              uint n = submode & 037;
              uint sgn = B & BIT15;
              B <<= n;
              B &= MASK15;
              B |= sgn;
              sim_debug (DBG_TRACE, & cpuDev, "  ASLB %d %06o->%06o\n", n, bsave, B);
              break;
            }

            if ((submode & 0740) == 0040) { // LRLB
              word16 bsave = B;
              uint n = submode & 037;
              for (uint i = 0; i < n; i ++) {
                uint bit = (B & BIT15) ? 1 : 0;
                B <<= 1;
                B |= bit;
              }
              sim_debug (DBG_TRACE, & cpuDev, "  LRLB %d %06o->%06o\n", n, bsave, B);
              break;
            }

            if ((submode & 0740) == 0140) { // LSRB
              uint n = submode & 037;
              B >>= n;
              break;
            }

            if ((submode & 0740) == 0200) { // ASLA
              uint n = submode & 037;
              uint sgn = A & BIT15;
              A <<= n;
              A &= MASK15;
              A |= sgn;
              break;
            }

            if ((submode & 0740) == 0200) { // ASRB
              uint n = submode & 037;
              uint sgn = B & BIT15;
              for (uint i = 0; i < n; i ++) {
                B >>= 1;
                B |= sgn;
              }
              break;
            }

            if ((submode & 0740) == 0240) { // LRLA
              uint n = submode & 037;
              for (uint i = 0; i < n; i ++) {
                uint bit = (A & BIT15) ? 1 : 0;
                A <<= 1;
                A |= bit;
              }
              break;
            }

            if ((submode & 0740) == 0300) { // ASRA
              uint n = submode & 037;
              uint sgn = A & BIT15;
              for (uint i = 0; i < n; i ++) {
                A >>= 1;
                A |= sgn;
              }
              break;
            }

            if ((submode & 0740) == 0340) { // LSRA
              uint n = submode & 037;
              A >>= n;
              break;
            }

            if ((submode & 0740) == 0400) { // LASL
              uint n = submode & 037;
              uint asgn = (A & BIT15);
              uint bsgn = (B & BIT15);
              uint64_t tmp = (((uint64_t) (A & MASK15)) << 15) | (B & MASK15);
              tmp <<= n;
              A = ((tmp >> 15) & MASK15) | asgn;
              B = (tmp & MASK15) | bsgn;
              break;
            }

            if ((submode & 0740) == 0440) { // LLRL
              uint32_t save = (((uint32_t) B) << 16) | A;
              uint n = submode & 037;
              for (uint i = 0; i < n; i ++) {
                uint abit = (A & BIT15) ? 1 : 0;
                uint bbit = (B & BIT15) ? 1 : 0;
                A = A << 1;
                A |= bbit;
                B = B << 1;
                B |= abit;
              }
              uint32_t new = (((uint32_t) B) << 16) | A;
              sim_debug (DBG_TRACE, & cpuDev, "  LLRL %d %011o->%011o\n", n, save, new);
              break;
            }

            if ((submode & 0740) == 0540) { // LLSR
              uint n = submode & 037;
              uint64_t tmp = (((uint64_t) A) << 16) | B;
              tmp >>= n;
              A = (tmp >> 16) & MASK16;
              B = tmp & MASK16;
              break;
            }


            reason = fault (FLT_ILL);
            break;

          case 05: // Register change group
            if (submode == 000) // NOP
              break;

            uint conditional = (submode >> 8) & 1;
            uint step = (submode >> 6) & 3;
            uint srcReg = (submode >> 3) & 7;
            uint dstReg = submode & 7;

            sim_debug (DBG_TRACE, & cpuDev,
              "  Reg chg grp CND %u, STP %s, SRC %s, DST %s\n", 
              conditional, stepStr[step], regStr[srcReg], regStr[dstReg]);

            if (conditional && OF == 0)
              break;

            uint src = 0;
            if (srcReg & 4)
              src |= X;
            if (srcReg & 2)
              src |= B;
            if (srcReg & 1)
              src |= A;

            switch (step) {
               case 0:
                 break;
               case 1:
                 src = add16b (src, 1u, 0, & OF);
                 break;
               case 2:
                 src = ~src;
               case 3:
                 src = sub16b (src, 1u, 1u, & OF);
                 break;
            }

            if (dstReg & 4)
              X = src;
            if (dstReg & 2)
              B = src;
            if (dstReg & 1)
              A = src;

            break;

          case 06: // Extended-addressing instruction group (optional)
// XXX
            reason = fault (FLT_UNIMP);
            break;

          case 07: 
            if (submode == 0401) { // SOF
              OF = 1;
              break;
            }
            if (submode == 0400) { // ROF
              OF = 0;
              break;
            }
        }
        break;
      } // case 000 Control group

      case 001: // LDA
        computeAddress ();
        W = memory[L];
        R = W;
        A = R;
        break;

      case 002: // LDB
        computeAddress ();
        W = memory[L];
        R = W;
        B = R;
        break;

      case 003: // LDX
        computeAddress ();
        W = memory[L];
        R = W;
        X = R;
        break;

      case 004: // INR
        computeAddress ();
        W = memory[L];
        R = W;
        R = add16b (R, 1u, 0, & OF);
        W = R;
        memory[L] = W;
        break;

      case 005: // STA
        computeAddress ();
        R = A;
        W = R;
        memory[L] = W;
        sim_debug (DBG_TRACE, & cpuDev, "  STA [%06o] <= %06o\n", L, W);
        break;

      case 006: // STB
        computeAddress ();
        R = B;
        W = R;
        memory[L] = W;
        break;

      case 007: // STX
        computeAddress ();
        R = X;
        W = R;
        memory[L] = W;
        break;

      case 010: { // I/O instruction group
        uint mode = (U >> 9) & 07; // Extract sub-opcode
        uint submode = U & 0777; // sub-sub-opcode
        switch (mode) {
          case 00: // EXC
            sim_debug (DBG_TRACE, & cpuDev, "  EXC cmd %o devno %o\n", (submode >> 6) & 07, submode & 077);
            reason = ioExc (submode);
            break;

          case 01: // SEN
            sim_debug (DBG_TRACE, & cpuDev, "  SEN cmd %o devno %o\n", (submode >> 6) & 07, submode & 077);
            L = P; // Copy program counter to memory address register
            W = memory[L]; // Fetch from mmory
            P = (P + 1) & MASK16; // Increment PC

            word16 jmpAddr = W;

            word1 sensed;
            reason = ioSen (submode, & sensed);
            if (reason)
              break;
 
            if (! sensed)
              break;

            while (jmpAddr & BIT15) {
              L = jmpAddr;
              W = memory[L];
              jmpAddr = W & MASK16;
            }

            P = jmpAddr;

            break;

          case 02: { // Data transfer in group
            word16 data;
            if ((submode & 0700) == 0000) { // INE

              L = P; // Copy program counter to memory address register
              W = memory[L]; // Fetch from mmory
              P = (P + 1) & MASK16; // Increment PC
              word16 bufferAddr = W;

              reason = ioRead (submode & 077, & data);
              if (reason)
                break;
              while (bufferAddr & BIT15) {
                L = bufferAddr;
                W = memory[L];
                bufferAddr = W & MASK16;
              }
              L = bufferAddr;
              memory[L] = data;
              break;
            } // submode 0


            if ((submode & 0700) == 0100) { // INA
              reason = ioRead (submode & 077, & data);
              if (reason)
                break;
              A |= data;
              break;
            }

            if ((submode & 0700) == 0200) { // INB
              reason = ioRead (submode & 077, & data);
              if (reason)
                break;
              B |= data;
              break;
            }

            if ((submode & 0700) == 0500) { // CIA
              reason = ioRead (submode & 077, & A);
              break;
            }

            if ((submode & 0700) == 0600) { // CIB
              sim_debug (DBG_TRACE, & cpuDev, "  CIB devno %o\n", submode & 077);
              reason = ioRead (submode & 077, & B);
              break;
            }
            reason = fault (FLT_ILL);
            break;
          }

          case 03: { // Data transfer out group
            word16 data;
            if ((submode & 0700) == 0100) { // OAR
              reason = ioWrite (submode & 077, A);
              break;
            }

            if ((submode & 0700) == 0200) { // OBR
              reason = ioWrite (submode & 077, B);
              break;
            }

            if ((submode & 0700) == 0000) { // OME
              L = P; // Copy program counter to memory address register
              W = memory[L]; // Fetch from mmory
              P = (P + 1) & MASK16; // Increment PC
              word16 bufferAddr = W;
              while (bufferAddr & BIT15) {
                L = bufferAddr;
                W = memory[L];
                bufferAddr = W & MASK16;
              }
              L = bufferAddr;
              data = memory[L];
              reason = ioWrite (submode & 077, data);
              break;
            }
            reason = fault (FLT_ILL);
            break;

          default:
            reason = fault (FLT_ILL);
            break;
          } // case 03: Data transfer out group
          break;

        } // switch mode
        break;
      } // case 010 I/O instruction group

      case 011: // ORA
        computeAddress ();
        W = memory[L];
        R = W;
        R = (R | A) & MASK16;
        A = R;
        break;

      case 012: // ADD
        computeAddress ();
        W = memory[L];
        R = W;
        R = add16b (R, A, 0, & OF);
        A = R;
        break;

      case 013: // ERA
        computeAddress ();
        W = memory[L];
        R = W;
        R = (R ^ A) & MASK16;
        A = R;
        break;

      case 014: // SUB
        computeAddress ();
        W = memory[L];
        R = W;
        R = sub16b (R, A, 0, & OF);
        A = R;
        break;

      case 015: // ANA
        computeAddress ();
        W = memory[L];
        R = W;
        R = (R ^ A) & MASK16;
        A = R;
        break;

      case 016: // MUL
        reason = fault (FLT_UNIMP); // XXX
        break;

      case 017: // DIV
        reason = fault (FLT_UNIMP); // XXX
        break;
    } // switch opCode
  } while (reason == 0);
} // sim_instr

