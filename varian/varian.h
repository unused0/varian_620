#ifndef VARIAN_H
#define VARIAN_H 1

#include "sim_defs.h"

#include <stdbool.h>
#include <stdint.h>

# if defined (__MINGW64__) || \
    defined (__MINGW32__)  || \
    defined (__GNUC__)     || \
    defined (__clang_version__)
#  define NO_RETURN __attribute__ ((noreturn))
#  define UNUSED    __attribute__ ((unused))
# else
#  define NO_RETURN
#  define UNUSED
# endif

//extern bool mode18;
#define MODE18 0

#define MAX_MEM 32768
#define MEM_SZ 32768

#define MASK16 0177777u
#define MASK15 0077777u
#define BIT18 01000000u
#define BIT17 0400000u
#define BIT15 0100000u
#define BIT8 0000400
#define BIT7 0000200
#define BIT6 0000100
#define BIT5 0000040
#define BIT4 0000020
#define BIT3 0000010
#define BIT2 0000004
#define BIT1 0000002
#define BIT0 0000001
#define SIGN16 0100000u

typedef unsigned int uint;
typedef uint32_t word18;
typedef uint16_t word16;
typedef uint16_t word9;
typedef uint8_t word6;
typedef uint8_t word5;
typedef uint8_t word3;
typedef uint8_t word1;

extern word16 memory[MEM_SZ];
extern word16 P; // Program counter
extern word16 A; // Accumulator
extern word16 B; // Double length accumulator
extern word16 X; // Index register

// Buffer registers
extern word16 R; // Operand register
extern word16 U; // Instruction register
extern word5 S; // Shift register
extern word16 L; // Memory address register
extern word16 W; // Memory word register

extern word1 OF; // Overflow

extern word1 SS1; // Sense switch 1
extern word1 SS2; // Sense switch 2
extern word1 SS3; // Sense switch 3

enum FLT_CODE {FLT_NONE, FLT_UNIMP, FLT_ILL, FLT_DEVNO, FLT_DEVCMD};
#endif

